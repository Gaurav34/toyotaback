const mongoose = require("mongoose");
// const SurveyTypes = require("../graphql/types/surveys");
const SurveySchema = new mongoose.Schema(
  {
    userEmail:  String ,
    userName:  String ,
    userMemberiD:String,
    userPhoneNumber: String,
  
    time: { type: String}
  },
  {
    timestamps: true
  }
);

const timeModelLogins = mongoose.model("lastlogins", SurveySchema);
module.exports = timeModelLogins;
